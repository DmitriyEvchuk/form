package ua.bigchuk;

public interface DBProperty {

	String getUser();
	
	String getPass();
	
	String getDBName();
	
	String getHost();
	
	String getEncoding();

}
