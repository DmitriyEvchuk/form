package ua.bigchuk;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class DataBase {

	private Statement statement;
	private Connection connection;

	public DataBase(DBProperty config) {

		try {
			
			Class.forName("org.mariadb.jdbc.Driver");

			Properties properties = new Properties();

			properties.setProperty("user", config.getUser());
			properties.setProperty("password", config.getPass());
			properties.setProperty("useUnicode", "true");
			properties.setProperty("characterEncoding", config.getEncoding());

			connection = DriverManager.getConnection("jdbc:mysql://" + config.getHost() + "/" + config.getDBName(),
					properties);

			try {

				statement = connection.createStatement();
			} catch (SQLException e1) {

				connection.close();
			}

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public void addAllNote(Iterable<String> fields, Iterable<String> data, String tableName) {

		String sql = "INSERT INTO " + tableName + " (" + dataToString(fields, false) + ")" + "VALUES ("
				+ dataToString(data, true) + ")";

		try {

			statement.executeUpdate(sql);

		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	public ResultSet getAllNote(String tableName) {

		try {

			return statement.executeQuery("select * from " + tableName);
		} catch (SQLException e) {

			e.printStackTrace();
		}

		return null;
	}

	public void close() {

		try {
			statement.close();
			connection.close();

		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	private String dataToString(Iterable<String> data, boolean quotes) {

		String forSql = "";
		String quote = "";

		if (quotes) {
			quote = "\"";
		}

		for (String elem : data) {

			forSql = forSql + quote + elem + quote + ",";
		}

		return forSql.substring(0, forSql.length() - 1);
	}

}
