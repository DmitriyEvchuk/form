package ua.bigchuk;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class Handler extends HttpServlet {

	private DataBase base;
	private ServletContext sc;

	public void init(ServletConfig config) throws ServletException {

		sc = config.getServletContext();
		base = new DataBase(new ConfigReader(sc.getRealPath("/") + "\\WEB-INF\\DBConfig.xml"));
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html;charset=UTF-8");
		createWeb(response.getWriter(), getParamNames(request));

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf8");

		base.addAllNote(getParamNames(request), getData(request), "Persons");
		doGet(request, response);
	}

	public void destroy() {
		base.close();
	}

	private void createWeb(PrintWriter out, List<String> noteName) {

		out.write("<HTML><HEAD>");
		out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
		out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"CSS/ForTable.css\" />");
		out.write("<TITLE>���������</TITLE>");
		out.write("</HEAD><BODY>");
		out.write("<TABLE>");

		createTableTitle(out, noteName);
		createTableContent(out);

		out.write("</TABLE></BODY></HTML>");
		out.close();
	}

	private void createTableContent(PrintWriter out) {

		ResultSet set = base.getAllNote("Persons");
		try {
			while (set.next()) {

				out.write("<TR>");

				out.write("<TD>" + set.getString("Last_Name") + "</TD>");
				out.write("<TD>" + set.getString("Name") + "</TD>");
				out.write("<TD>" + set.getString("Telephone") + "</TD>");
				out.write("<TD>" + set.getString("eMail") + "</TD>");
				out.write("<TD>" + set.getString("Country") + "</TD>");
				out.write("<TD>" + set.getString("City") + "</TD>");
				out.write("<TD>" + set.getString("Date") + "</TD>");
				out.write("</TR>");
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	private void createTableTitle(PrintWriter out, List<String> noteName) {

		out.write("<TR>");

		for (String name : noteName) {

			out.write("<TH>" + name.toUpperCase() + "</TH>");
		}

		out.write("</TR>");
	}

	private List<String> getData(HttpServletRequest request) {

		List<String> data = new ArrayList<String>();

		Enumeration<String> names = request.getParameterNames();

		while (names.hasMoreElements()) {

			data.add(request.getParameter((String) names.nextElement()));
		}

		return data;
	}

	private List<String> getParamNames(HttpServletRequest request) {

		List<String> param = new ArrayList<String>();

		Enumeration<String> names = request.getParameterNames();

		while (names.hasMoreElements()) {

			param.add(names.nextElement());
		}

		return param;
	}

}
