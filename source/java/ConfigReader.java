package ua.bigchuk;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;

public class ConfigReader implements DBProperty {

	private Document doc;

	public ConfigReader(String path) {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			doc = db.parse(new File(path));
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

	public String getUser() {

		return doc.getElementsByTagName("user").item(0).getTextContent();
	}

	public String getPass() {
		
		return doc.getElementsByTagName("password").item(0).getTextContent();
	}

	public String getDBName() {
	
		return doc.getElementsByTagName("DBname").item(0).getTextContent();
	}

	public String getHost() {
		
		return doc.getElementsByTagName("host").item(0).getTextContent();
	}

	public String getEncoding() {
		
		return doc.getElementsByTagName("encoding").item(0).getTextContent();
	}

}
