/*
 *
 */
var fieldsContext = {
	"last_Name" : "фамилия",
	name : "имя",
	telephone : "телефонный номер",
	city : "город",
	country : "страна",
	email : "eMail"
};

var fieldsValid = {
	"last_Name" : textValidator,
	name : textValidator,
	telephone : phoneValidator,
	city : textValidator,
	country : textValidator,
	email : eMailValidator
};

var message = document.querySelector("#message");

document.querySelector("form").addEventListener("submit", function(event) {

	var valid = true;

	Array.prototype.every.call(event.target.elements, function(field) {

		if (field.value != "Отправить" && field.name != "date")
			valid = fieldsValid[field.name](field);

		return valid;
	});

	if (!valid) {

		message.style.display = "block";

		 setTimeout(function() {
		 	message.style.display = "none";
		 }, 3000);

		event.preventDefault();
	}
});

function textValidator(field) {

	if (!/^([A-ZА-Я])([a-zа-я])+$/.test(field.value)) {

		message.innerHTML = "Поле "
				+ fieldsContext[field.name]
				+ " должно содержать<br/>только буквы и начинаться с большой буквы!"

		return false;
	}

	return true;
}

function phoneValidator(field) {

	if (!/^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$/.test(field.value)) {

		message.innerHTML = "Поле " + fieldsContext[field.name]
				+ "<br/>должно иметь формат XXX-XXX-XX-XX"

		return false;
	}

	return true;
}

function eMailValidator(field) {

	if (!/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/
			.test(field.value)) {

		message.innerHTML = "Не правильный формат.Заполните правильно поле"
				+ fieldsContext[field.name]
				+ ".<br/>Например admin@wisdomweb.ru  "

		return false;
	}

	return true;
}
