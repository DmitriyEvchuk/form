CREATE DATABASE IF NOT EXISTS testDB;

use testDB;

CREATE TABLE IF NOT EXISTS Persons
(
ID int NOT NULL AUTO_INCREMENT,
Last_Name varchar(255),
Name varchar(255),
Telephone varchar(255),
eMail varchar(255),
Country varchar(255),
City varchar(255),
Date date,
PRIMARY KEY (ID)

) CHARACTER SET UTF8;